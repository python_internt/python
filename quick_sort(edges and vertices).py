import time
import random

# open files with graph information
path_vertices = input("Insert path to vertices file: ")
vertices = open(path_vertices, "r", encoding = 'utf_8')
path_edges = input("Insert path to edges file: ")
edges = open(path_edges, "r", encoding = 'utf_8')
# import files in the lists
vertices_list = list(vertices.read().splitlines())
edges_list = list(edges.read().splitlines())

# start time counter
start = time.time()
# quick_sort functions
def q_sort(input_list):
    #statement for sorting
   if len(input_list) <= 1:
       return input_list
   else:
       #choose random value for comparison
       some_value = random.choice(input_list)
       less = [x for x in input_list if x < some_value ]
       equal = [x for x in input_list if x == some_value]
       bigger = [x for x in input_list if x > some_value]
       return q_sort(less) + equal + q_sort(bigger)

# new sorted lists
sorted_vertices = q_sort(vertices_list)
sorted_edges = q_sort(edges_list)
# Output information
elementsCount = int(input("Count of elements to output:  "))
print("Sorted vertices: " , sorted_vertices[0:elementsCount] , '\n')
print("Sorted edges: " , sorted_edges[0:elementsCount])
print ("Time spent: ", time.time() - start, "seconds.")
input("Press enter to exit...")


